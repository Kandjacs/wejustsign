Roadmap WeJustSign
==================


## ToDo

[x] Schéma de bases de données

[x] Création d'entities

[x] Wireframe

[x] DB Flow

[x] Migrations

[x] Fixtures

[X] **Homepage**
  - [X] Connexion -M
  - [X] Création -M
  - [X] Sécurité -M

[] **Apprenant**
  - [x] Formulaire pour validation du code
  - [x] Page de validation

[] **Formateur**
  - [x] Formateur homePage
  - [x] Page de génération de code
  - [x] Validation de sa propre présence
  - [x] **CRUD Time slot** Actuel/Précédent
    - [x] READ : Attendance
    - [] UPDATE : Attendance

[] **Responsable**
  - [] Responsable homePage
  - [x] **CRUD : Formations** 
    - [x] CREATE : new Formation -L
    - [x] UPDATE : Formation -L
    - [x] DELETE : Formation -L
  - [] **CRUD : Promo** 
    - [x] CREATE : new Promo -K
    - [x] READ : Afficher élèves/Promo **CRUD USER** -K
    - [] READ : Afficher TimeSlots/Promo **CRUD TIME_SLOT**
    - [x] UPDATE : Promo -K
    - [x] DELETE : Promo -K
    - [x] Afficher la formation de la promo
  - [X] **CRUD : USER**
    - [X] CREATE : Créer Users -M
    - [X] UPDATE : User, Promo -M
    - [X] DELETE : Supprimer -M
  - [] **CRUD : Time_Slot** / Promo ++ Sélection de fenêtre de temps
    - [x] CREATE : Time_Slot -L
      - [] CREATE : Améliorer la création des timeslots
    - [] READ : Attendance / Users
      - [x] UPDATE : Attendance
    - [x] UPDATE : Time_Slot
    - [x] DELETE : Time_Slot -L

<br>
<br>

## DataBase

### PAGE CONNEXION 

  * Bloc connexion Mail et MDP
  * Bouton de validation + **import données de connexion**
      => Verification des données **import des données USER**
      => Redirection vers page en fonction du rôle 
  * Bouton renvoi vers page création de compte


### PAGE CREATION COMPTE 

  * Bloc Nom
  * Bloc Prénom 
  * Bloc Mail 
  * Blocs Mot de passe + confirmation 
      => **Vérifier qu'ils soient identiques**
  * Bouton **Envoi dans USER** si tout les champs valides


### PAGE APPRENANTS

  * Bloc Identification **Import des données USER**
  * Bloc Créneau actuel **Import des données TimeSlot**
  * Bloc Code **Import du code**
  * Bouton envoi 
    #### PAGE CONFIRMATION
      * Confirmation si code bon + **Envoi dans Check**
  * Message d'erreur si code erroné


### PAGE FORMATEUR 

  * Bloc Identification **Import des données USER**
  * Bloc Créneau actuel **Import des données TimeSlot**
  * Génération du code **Envoi dans TimeSlot**
  * Validation de la présence **Envoi dans CHECK** + Récupération données bloc Identification + TimeSlot
  * Bouton CRUD TimeSlot **Récupération ID formateur**
      => *VOIR CRUD TimeSlot*


### PAGE RESPONSABLE 

  * Bloc Identification **Import des données USER**
  * Bloc Créneau actuel **Import des données TimeSlot**
  * CRUD Création Promo
    * Bouton CRUD USER BY SESSION
    * Bouton CRUD TimeSlot par promo
  * CRUD User List
  * CRUD Formation

<br>
<br>

# Planning

## En cours

+ Kévin
    >Gestion du code / Signature

+ Mathieu

    >Timeslots V2 

+ Loic

    >Form constraints

## Restant

### Reste à faire

- [] Gestion des pages/rôles
- [] Pages d'accueil par rapport au rôle
- [] Création des voters
  - [] Suppression de Promo indisponible si des Timeslots ont été validés
  - [] Accès pour le formateur à la présence des apprenants uniquement sur le timeslot actuel et précédent, et quand il veut pour le Responsable
  - [] Troisième Voter - A definir
- [] Visualisation de la présence des élèves
- [] NavBar avec visualisation du user connecté

## Planning

### **Lundi**

  K -> Fin de la Gestion des signatures

  M -> Timeslots V2

  L -> Fin des Form Constraints

### **Mardi**

  K -> Visualisation de la présence des élèves

  M -> Fin des Timeslots V2

  L -> 

### **Mercredi**

  K -> Pages d'accueil par rapport au rôle

  M -> Gestion des pages/rôles

  L ->

### **Jeudi**

  K -> Pages d'acceuil par rapport aux rôles / Navbar avec visualisation du user connecté

  M -> Fin de gestion des pages rôles

  L -> Voters

### **Vendredi**

  K -> Voters

  M -> Voters

  L -> Voters