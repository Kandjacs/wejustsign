Brief projet "Signature" 
========================

Ce projet se déroulera sur **5 semaines**, il doit permettre de découvrir le framework Symfony. Il consistera à construire une application de signature électronique dans le cadre d'une formation.

L'application sera utilisée par trois types d'utilisateurs différents : les apprenant-es, les formateurs et le responsable. Il devra permettre de valider la présence des apprenant-es / formateurs sur des créneaux de demi-journée.


## Objectifs

* Prise en main de Symfony (Entités, formulaires, sécurité, moteur de template twig)
* Identifier problématique(s) et proposer une solution 
* Imaginer et concevoir une application
* Gestion de projet (tâches, planning,..)
* Gérer des utilisateurs avec différents rôles et droits sur un back-end


## Compétences visées

* Compétence 1, maquettage : niveau transposer
* Compétence 2, web statique : niveau transposer
* Compétence 5, créer une base de donnée : niveau adapter
* Compétence 6, développer les composants d'accès aux données : niveau adapter
* Compétence 7, développer la partie back-end d'une application : niveau adapter


## Modalités pédagogiques

Ce projet se déroulera sur 5 semaines en 3 sprints, le travail sera réalisé par groupe :

* Sprint 1 - semaine du 29/03 au 02/04 : réflexions sur la problématique, architecturation du projet, début d'implémentation
* Sprint 2 - semaine du 06/04 au 09/04
* Atelier backoffice / Sylius / Préparation sprint 3 - semaine du 19/04 au 23/04
* Sprint 3 - semaine 26/04 au 12/05


## Tâches


### Description
L'application devra avoir :

* une page de connexion
* différents rôles avec les fonctionnalités associées

Il y a trois types d'utilisateurs : apprenant-es, formateurs, responsables.


#### Le responsable
* Il définit des créneaux de formation, affecte les apprenant-es et formateurs sur les créneaux
* Il peut sortir des fiches de présence par créneau/semaine


#### Le formateur
* Il donne un code aux apprenant-es qui leur permet de valider un créneau
* Il peut spécifier si un-e absent-e était en fait présent-e


#### Les apprenant-es
* Il peuvent signer en utilisant le code fourni par le formateur


### Sprint 1

1. Réfléchir au fonctionnement, définir les fonctionnalités et le schéma de données associés
2. Proposer des wireframes pour :
* La page de connexion
* Les pages d'accueil apprenant-es, formateur, responsable
* Le suivi des présences (responsable, formateur) et la définition des créneaux pour le responsable
3. Définir un planning des tâches


### Sprint 2

Finaliser les tâches énumérées précédemment


## Contraintes
Chaque apprenant-es devra avoir construit au moins une entité et un formulaire.