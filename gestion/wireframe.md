WIREFRAME
=========


# Homepage

+ Page de **login**
    * Adresse Mail
    * Mot de passe
    * Role

+ Page de **création de compte**
<hr>
<hr>

## /Apprenant

+ Page pour **rentrer le code du slot**
    * champs pour code
    * validation

     -> /validé

<hr>

## /Formateur

+ Page pour **générer un code**
    * Bouton pour générer un code et l'afficher
    * Bouton pour valider sa propre présence

+ **CRUD Time slot** : Accéder au timeSlot actuel et précédent
    * READ : Voir les apprenants présents/absents
    * UPDATE : Modifier la présence des apprenants

<hr>

## /Responsable

+ **CRUD Promo** => Affiche toutes les promos
    * *CREATE* : Créer une nouvelle promo  __
    * *READ* : Afficher les élèves de la promo
      - **CRUD USER** by promo
    * *READ* : **CRUD TIME_SLOT** by promo
    * *UPDATE* : Modifier la promo
    * *DELETE* : Supprimer une promo

+ **CRUD Time_slot** => Afficher les time slots de la promo
    * *CREATE* : Créer un nouveau time slot
    * *READ* : Afficher la présence/absence des élèves de la promo
      - *UPDATE* : Modifier la présence des élèves
    * *UPDATE* : Modifier les détails de la time slot
    * *DELETE* : Supprimer une time slot

+ **CRUD User** => Afficher les users
    * *CREATE* : Créer un nouveau user
    * *UPDATE* : Modifier un user [Ajouter une promo à un user]
    * *DELETE* : Supprimer un user

+ **CRUD Formation** => Affiche toutes les formations
    * *CREATE* : Créer une nouvelle formation
    * *UPDATE* : Modifier une formation
    * *DELETE* : Supprimer une formation