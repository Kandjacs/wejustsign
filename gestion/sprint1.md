# Brief sprint 1

*semaine du 29/03 au 02/04*

* Réflexions sur la problématique
* Architecturation du projet
* Début d'implémentation

## Problématique 

Construire une application de signature électronique.

#### Spécificités

3 utilisateurs différents : 
  * Responsable
      - Défini les créneaux de formation
      - Affect les apprenants et formateurs sur les créneaux
      - Permet de sortir les fiches de présence
  * Formateur
      - Donne un code aux apprenants
      - Rattrapage de signature
  * Apprenant
      - Signer en utilisant le code du formateur

Code généré QUE pour le formateur/reponsable


## Architecture

PAGE DE CONNEXION
      |
      |__**Page Responsable**
      |     \ CRUD année
      |     \ CRUD créneaux   [CREATE : Assigner un professeur à un créneau || READ : CRUD présence || UPDATE : Modifier prof || DELETE ]
      |              \ Impression par semaine                                   |
      |     \ _Création d'utilisateur_                                          |_[C: || R: Afficher la présence des élèves]
      |    ++ _Génération de code_                                              |_[U: Modifier la présence d'un élève || D:]
      |                                                                         |
      |__**Page Formateur**                                                     |
      |     \ Générer un Code                                                   |
      |     \ CRUD journée     _________________________________________________|
      |   ++ _Signature présence_
      |
      |__**Page Apprenant**
      |     \ Signature présence [RENTRER LE CODE || SIGNATURE]

