# Presentation

Projet Réalisé par Mathieu, Loic et Kévin


## Features

Création de Formations
Création de Promos
Création d'utilisateurs ( Utilisateur crée son compte, puis le responsable l'assigne à une promo )
Création de Timeslots ( uns par uns )


### Apprenant 

Validation du cours actuel


### Formateur

Génération du code pour le cours actuel pour la promo

Visualisation de la présence du cours actuel

Visualisation de la présence des 2 cours précedents


### Responsable

Visualisation des Promos

Visualisation des Formations

Visualisation des  Utilisateurs

Visualisation des Timeslots