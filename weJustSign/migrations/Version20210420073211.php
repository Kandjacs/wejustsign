<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210420073211 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE `check` (id INT AUTO_INCREMENT NOT NULL, timeslot_id INT NOT NULL, user_id INT DEFAULT NULL, attendance VARCHAR(255) DEFAULT NULL, INDEX IDX_3C8EAC13F920B9E9 (timeslot_id), INDEX IDX_3C8EAC13A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE formation (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE promo (id INT AUTO_INCREMENT NOT NULL, formation_id INT NOT NULL, startyear DATETIME NOT NULL, endyear DATETIME NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_B0139AFB5200282E (formation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE promo_user (promo_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_C70A754FD0C07AFF (promo_id), INDEX IDX_C70A754FA76ED395 (user_id), PRIMARY KEY(promo_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE timeslot (id INT AUTO_INCREMENT NOT NULL, promo_id INT DEFAULT NULL, startdate DATETIME NOT NULL, enddate DATETIME NOT NULL, code INT DEFAULT NULL, INDEX IDX_3BE452F7D0C07AFF (promo_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, mail VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D6495126AC48 (mail), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `check` ADD CONSTRAINT FK_3C8EAC13F920B9E9 FOREIGN KEY (timeslot_id) REFERENCES timeslot (id)');
        $this->addSql('ALTER TABLE `check` ADD CONSTRAINT FK_3C8EAC13A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE promo ADD CONSTRAINT FK_B0139AFB5200282E FOREIGN KEY (formation_id) REFERENCES formation (id)');
        $this->addSql('ALTER TABLE promo_user ADD CONSTRAINT FK_C70A754FD0C07AFF FOREIGN KEY (promo_id) REFERENCES promo (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE promo_user ADD CONSTRAINT FK_C70A754FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE timeslot ADD CONSTRAINT FK_3BE452F7D0C07AFF FOREIGN KEY (promo_id) REFERENCES promo (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE promo DROP FOREIGN KEY FK_B0139AFB5200282E');
        $this->addSql('ALTER TABLE promo_user DROP FOREIGN KEY FK_C70A754FD0C07AFF');
        $this->addSql('ALTER TABLE timeslot DROP FOREIGN KEY FK_3BE452F7D0C07AFF');
        $this->addSql('ALTER TABLE `check` DROP FOREIGN KEY FK_3C8EAC13F920B9E9');
        $this->addSql('ALTER TABLE `check` DROP FOREIGN KEY FK_3C8EAC13A76ED395');
        $this->addSql('ALTER TABLE promo_user DROP FOREIGN KEY FK_C70A754FA76ED395');
        $this->addSql('DROP TABLE `check`');
        $this->addSql('DROP TABLE formation');
        $this->addSql('DROP TABLE promo');
        $this->addSql('DROP TABLE promo_user');
        $this->addSql('DROP TABLE timeslot');
        $this->addSql('DROP TABLE user');
    }
}
