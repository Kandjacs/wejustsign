<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210419114756 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE user_promo');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user_promo (user_id INT NOT NULL, promo_id INT NOT NULL, INDEX IDX_52D161A3A76ED395 (user_id), INDEX IDX_52D161A3D0C07AFF (promo_id), PRIMARY KEY(user_id, promo_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE user_promo ADD CONSTRAINT FK_52D161A3A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_promo ADD CONSTRAINT FK_52D161A3D0C07AFF FOREIGN KEY (promo_id) REFERENCES promo (id) ON UPDATE NO ACTION ON DELETE CASCADE');
    }
}
