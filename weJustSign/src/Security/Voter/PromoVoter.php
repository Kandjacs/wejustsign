<?php

namespace App\Security\Voter;
use App\Entity\User;
use App\Entity\Promo;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class PromoVoter extends Voter
{
    const VIEW = 'view';
    const EDIT = 'edit';

    protected function supports($attribute, $subject)
    {
        return in_array($attribute, [
                    self::EDIT,
                    self::VIEW
                ]) && $subject instanceof \App\Entity\Promo;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            return false;
        }
        $roles = $user->getRoles();
        
        switch ($attribute) {
            case self::EDIT:
            case self::VIEW: {
                if( !in_array("ROLE_RESPONSABLE", $roles) ) {
                    return false;
                }
                
                return $this->isOwnPromo($subject, $user);
            } break;
        }
        return false;
    }

    private function isOwnPromo(Promo $promo, User $user): bool
    {       
        foreach ($promo->getUsers() as $users) {
            if($user->getId() === $users->getId()) {
                return true;
            }
        }
        return false;
    }

}
