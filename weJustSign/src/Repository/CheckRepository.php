<?php

namespace App\Repository;

use App\Entity\Check;
use App\Entity\Timeslot;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Check|null find($id, $lockMode = null, $lockVersion = null)
 * @method Check|null findOneBy(array $criteria, array $orderBy = null)
 * @method Check[]    findAll()
 * @method Check[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CheckRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Check::class);
    }


    public function findByTimeslot($start, $end) // route : '/timeslot/checks/by/date' || Pour l'instant inutile, on garde quand même, ca servira
    {
        return $this->createQueryBuilder('c')
            ->select('c.attendance', 'u.firstname', 'u.lastname', 't.startdate', 't.enddate')
            ->where('t.startdate = :start')
            ->andWhere('t.enddate = :end')

            ->join('c.timeslot', 't')
            ->join('c.user', 'u')

            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->getQuery()
            ->getResult()
        ;
    }

    public function queryCheckTimeslot($timeslot){
        return $this->createQueryBuilder('c')
            ->select('c.attendance', 'u.firstname', 'u.lastname', 'u.roles', 't.startdate', 't.enddate')
            ->andWhere('c.timeslot = :timeslot')

            ->join('c.user', 'u')
            ->join('c.timeslot', 't')

            ->setParameter('timeslot', $timeslot)
            ->getQuery()
            ->getResult()
            ;
    }

    public function queryByUserAndTimeslot(User $user, Timeslot $timeslot){
        return $this->createQueryBuilder('c')
            ->select('c')
            ->where('c.user = :user')
            ->andWhere('c.timeslot = :timeslot')
            ->setParameter('timeslot', $timeslot)
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }
    // /**
    //  * @return Check[] Returns an array of Check objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Check
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
