<?php

namespace App\Repository;

use App\Entity\Promo;
use App\Entity\Timeslot;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Validator\Constraints\Date;

/**
 * @method Timeslot|null find($id, $lockMode = null, $lockVersion = null)
 * @method Timeslot|null findOneBy(array $criteria, array $orderBy = null)
 * @method Timeslot[]    findAll()
 * @method Timeslot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TimeslotRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Timeslot::class);
    }


    public function timeslotByDatePromo($dateNow, $userPromo)
    {
        return
        $this->createQueryBuilder('t')
            -> select('t')

            ->setParameter('dateNow', $dateNow)
            ->setParameter('userPromo', $userPromo)

            ->where('t.promo = :userPromo')
            ->andwhere('t.startdate < :dateNow')
            ->andWhere('t.enddate > :dateNow')

            ->getQuery()
            ->getResult()

        ;
       
    }

    public function precendentTimeslotsByPromo( DateTime $startTimeslot, Promo $userPromo, int $resultNumber): ?array
    { 
        return
            $this
                ->createQueryBuilder('t')
                ->select('t')

                ->setParameter('startTimeslot', $startTimeslot)
                ->setParameter('userPromo', $userPromo)

                ->where('t.enddate < :startTimeslot')
                ->andWhere('t.promo = :userPromo')

                ->orderBy('t.startdate', 'DESC')

                ->setMaxResults($resultNumber)

                ->getQuery()
                ->getResult() // Ce Result doit contenir les 2 timeslots précédents celui actif dans les paramètes.
        ;
    }

    // /**
    //  * @return Timeslot[] Returns an array of Timeslot objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Timeslot
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
