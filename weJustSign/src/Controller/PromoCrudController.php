<?php

namespace App\Controller;

use App\Security\Voter\PromoVoter;
use App\Entity\Promo;
use App\Form\PromoIndexFilter;
use App\Form\PromoType;
use App\Repository\PromoRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\VarDumper\Cloner\Data;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Security;



/**
 * @Route("/promo/crud")
 */
class PromoCrudController extends AbstractController
{
    /**
     * @Route("/", name="promo_crud_index", methods={"GET", "POST"})
     */
    public function index(PromoRepository $promoRepository, PaginatorInterface $paginator, Security $security ,Request $request ): Response
    {
        $seeAllBool = $request->query->get("seeAllInit", false);
        $user = $security->getUser();
        $page = 1;
        
        $filters = $this->createForm(PromoIndexFilter::class);
        $filters->handleRequest($request);

        if($filters->isSubmitted() && $filters->isValid()) {
            $seeAllBool = $filters->getData()["seeAll"];
        } else {
            $page = $request->query->getInt('page', 1);
        }

        $promoData = $promoRepository->queryMyPromos($user, $seeAllBool);

        $promos = $paginator->paginate(
            $promoData,
            $page,
            5
        );

        return $this->render('promo_crud/index.html.twig', [
            'promos' => $promos,
            'filters' => $filters->createView(),
            'filterParams' => ["seeAllInit" => $seeAllBool]
        ]);
    }

    /**
     * @Route("/new", name="promo_crud_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $promo = new Promo();
        $form = $this->createForm(PromoType::class, $promo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($promo);
            $entityManager->flush();

            return $this->redirectToRoute('promo_crud_index');
        }

        return $this->render('promo_crud/new.html.twig', [
            'promo' => $promo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="promo_crud_show", methods={"GET"})
     */
    public function show(Promo $promo): Response
    {
        return $this->render('promo_crud/show.html.twig', [
            'promo' => $promo,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="promo_crud_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Promo $promo): Response
    {
       
        $this->denyAccessUnlessGranted(PromoVoter::EDIT, $promo);
        
        $form = $this->createForm(PromoType::class, $promo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('promo_crud_index');
        }

        return $this->render('promo_crud/edit.html.twig', [
            'promo' => $promo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="promo_crud_delete", methods={"POST"})
     */
    public function delete(Request $request, Promo $promo): Response
    {
        if ($this->isCsrfTokenValid('delete'.$promo->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($promo);
            $entityManager->flush();
        }

        return $this->redirectToRoute('promo_crud_index');
    }
}


