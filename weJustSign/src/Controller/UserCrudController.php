<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserCrudEditFormType;
use App\Repository\UserRepository;
use App\Repository\PromoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/user/crud")
 */
class UserCrudController extends AbstractController
{
    /**
     * @Route("/", name="user_crud_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository, PromoRepository $promoRepository): Response
    {
        return $this->render('user_crud/index.html.twig', [
            'users' => $userRepository->findAll(),
            'promos' => $promoRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_crud_show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        return $this->render('user_crud/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * 
     * @Route("/{id}/edit", name="user_crud_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user, PromoRepository $promoRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_RESPONSABLE');
        $form = $this->createForm(UserCrudEditFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_crud_index');
        }

        return $this->render('user_crud/edit.html.twig', [
            'user' => $user,
            'promos' => $promoRepository->findAll(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_crud_delete", methods={"POST"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_crud_index');
    }
}
