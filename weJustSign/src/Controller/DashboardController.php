<?php

namespace App\Controller;

use App\Entity\Check;
use App\Entity\Timeslot;
use App\Entity\User;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class DashboardController extends AbstractController
{
    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function Dashboard(Security $security): Response
    {
        /**
         * @var User
         */
        $userConnected = $security->getUser();
        $userRole = $userConnected->getRoles();
        $userPromos = $userConnected->getPromos();
        $dateNow = new DateTime;




        $apprenant = 'ROLE_APPRENANT';
        $formateur = 'ROLE_FORMATEUR';
        $responsable = 'ROLE_RESPONSABLE';

        $rolesCount = count($userRole);

        if ( $rolesCount === 1 &&
        $userRole[0] === $apprenant) {

            $timeslot = $this->getDoctrine()
            ->getRepository(Timeslot::class)
            ->timeslotByDatePromo($dateNow, $userPromos);


            return $this->render('dashboard/apprenant/index.html.twig', [
                'dateNow' => $dateNow,
                'timeslot' => $timeslot,
                ]);
        }
            
            
            if ( $rolesCount === 1 &&
            $userRole[0] === $formateur) {

                $timeslot = $this->getDoctrine()
                ->getRepository(Timeslot::class)
                ->timeslotByDatePromo($dateNow, $userPromos);


                $resultNumber = 2;
                if ($timeslot !== []){
                $startTimeslot = $timeslot[0]->getStartDate();
                $precedentTimeslots = $this->getDoctrine()
                ->getRepository(Timeslot::class)
                ->precendentTimeslotsByPromo($startTimeslot, $userPromos[0], $resultNumber);  // La fonction de filtre doit retourner un array de deux timeslot précédents celui actif.
            }else{
                $precedentTimeslots = null;
            }
                return $this->render('dashboard/formateur/index.html.twig', [
                'dateNow' => $dateNow,
                'timeslot' => $timeslot,
                'precedentTimeslots' =>$precedentTimeslots,
                'resultNumber' => $resultNumber
                ]);
            }

            if ( $rolesCount === 1 &&
            $userRole[0] === $responsable) {

            return $this->render('dashboard/responsable/index.html.twig', [

            ]);
            }

      return $this->render('dashboard/index.html.twig');


    }




    /**
     * @Route("/formateur/check/{id}", name="formateur_check", methods={"GET"})
     */
    public function formateurCheck( Request $request ,Timeslot $timeslot): Response

    {
        // DOIT INTEGRER UNE FONCTION POUR CHANGER LA PRESENCE DES ELEVES
        $checks = $this->getDoctrine()
        ->getRepository(Check::class)
        ->queryCheckTimeslot($timeslot)
        ;


        return $this->render('dashboard/formateur/check.html.twig', [
            'checks' => $checks,
            'timeslot' =>$timeslot
        ]);
    }













}