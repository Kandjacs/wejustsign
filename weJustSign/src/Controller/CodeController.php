<?php

namespace App\Controller;

use App\Entity\Check;
use App\Entity\Timeslot;
use App\Entity\User;
use App\Form\CodeCheckType;
use App\Form\CodeGenType;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;



class CodeController extends AbstractController
{


    /**
     * @Route("/codeGenerator/{id}", name="codeGenerator", methods={"GET","POST"})
     */
    public function codeGenerator(Request $request ,Timeslot $timeslot, Security $security): Response
    {
        $code = random_int(10000, 99999);
        $formateur = $security->getUser();
        


        if( $timeslot->getCode() === null )
        {
            $timeslot->setCode($code);
        } 

        $form = $this->createForm( CodeGenType::class, $timeslot );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $promo = $timeslot->getPromo();
            $users = $promo->getUsers();
            $check = new Check;
            $check
                ->setTimeslot($timeslot)
                ->setUser($formateur)
                ->isPresent();
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($check);
                

            foreach ($users as $user) {
                if ($user !== $formateur){
                $check = new Check;
                $check
                    ->setTimeslot($timeslot)
                    ->setUser($user)
                    ->isWaiting();

                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($check);
                    $entityManager->flush();
                }
            }



            $this->getDoctrine()->getManager()->flush();
            return $this->render('code/generator/code_generated.html.twig', [
                'timeslot' => $timeslot
            ]);
        }

        return $this->render('code/generator/index.html.twig', [
            'timeslot' => $timeslot,
            'form' => $form->createView(),

        ]);
    }

    /**
     * @Route("/codeValidator/{id}", name="codeValidator", methods={"GET","POST"})
     */
    public function codeValidator( Timeslot $timeslot, Security $security, Request $request): Response
    {

        // Get the connected UserId
        /**
         * @var User
         */
        $user = $security->getUser();
        $idUser = $user->getId();
        
        
        // Get the requested User
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($idUser);




        $startDate = $timeslot->getStartDate();
        $endDate = $timeslot->getEndDate();
        $todayDate = new DateTime('now');


        $check = new Check;
        $check
            ->setUser($user)
            ->setTimeslot($timeslot);
            
        $form = $this->createForm(CodeCheckType::class, $check);
        $form->handleRequest($request);

        
        if($form->isSubmitted()){
            
            $actualCode = $form->getData()->getTimeslot()->getCode();
            $enteredCode = $form->get('code')->getData();
            
            if($actualCode === $enteredCode){

                // Changer l'attendance du check
                $check->isPresent();

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($check);
                $entityManager->flush();
                $this->getDoctrine()->getManager()->flush();

                return $this->render('code/validator/code_validated.html.twig');

            }else{

                $this->addFlash(
                    'error',
                    'Code erroné. Veuillez entrer un code valide.'
                );
            }
        }
        
        return $this->render('code/validator/index.html.twig', [
            'check' => $check,
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

}