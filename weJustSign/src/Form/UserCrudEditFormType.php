<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Promo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UserCrudEditFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname')
            ->add('lastname')
            ->add('mail')
            ->add('promos', EntityType::class, [
                'class' => Promo::class,
                'expanded' => true,
                'multiple' => true,
                'by_reference' => false,
            ])
            ->add('roles', ChoiceType::class, [
                'choices' => [
                    'Apprenant' => 'ROLE_APPRENANT',
                    'Formateur' => 'ROLE_FORMATEUR',
                    'Responsable' => 'ROLE_RESPONSABLE',
                ],
                'expanded'  => true, // liste déroulante
                'multiple'  => false, // choix multiple
            ]);
            
        
        

        $builder->get('roles')
        ->addModelTransformer(new CallbackTransformer(
            function ($rolesArray) {
                // transform the array to a string
                return count($rolesArray)? $rolesArray[0]: null;
            },
            function ($rolesString) {
                // transform the string back to an array
                return [$rolesString];
            }
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

}
