<?php

namespace App\Form;

use App\Entity\Promo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PromoIndexFilter extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $builder
          ->add('seeAll', ChoiceType::class, 
          [
            'expanded' => true,
            'multiple' => false,
            'choices' => 
              [
                'Voir mes promos' => false,
                'Voir toutes les promos' => true
              ],
            // 'required' => true,
            'empty_data' => 'Voir mes promos'
          ]
          )
          ->add('submit', SubmitType::class );
    }




    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([

        ]);
    }
}