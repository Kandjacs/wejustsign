<?php

namespace App\DataFixtures;

use App\Entity\Check;
use App\Entity\Formation;
use App\Entity\User;
use App\Entity\Promo;
use App\Entity\Timeslot;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use \Faker\Factory;
class AppFixtures extends Fixture
{

    private $faker;

    public function __construct(){
        $this->faker = Factory::create('fr_FR');
    }




    private function createRandomRole($rand){
        $role = [];
        if ($rand === 0) {
            $role[] = "Apprenant";
        }
        elseif($rand === 1) {
            $role[] = "Formateur";
        }
        elseif($rand === 2){
            $role[] = "Responsable";
        }

        return $role;
    }




    private function createRandomAttendance($rand){
        $presence = "";
        if ( $rand === 0 ){ return $presence = "présent"; }
        elseif( $rand === 1 ) {return $presence = "absent"; }

        return $presence;
    }

    



    public function loadFormation(ObjectManager $manager){
        for($k = 0; $k <= 2; $k++)
        {
            $formation = new Formation;
            $formation
                ->setName( $this->faker->word());
            
            $manager->persist($formation);
        }
        $manager->flush();
    }



    public function loadUser(ObjectManager $manager, $promos){
        for($i = 0; $i <= 20; $i++)
        {
            $randA = rand(0, 2);
            $randB = rand(0, 3);

            $user = new User();
            $user
              ->setFirstname($this->faker->firstName())
              ->setLastname($this->faker->lastName())
              ->setMail($this->faker->email())
              ->setPassword($this->faker->password())
              ->setRoles( $this->createRandomRole($randA) )
              ->addPromo($promos[$randB]);
            $manager->persist($user);
        };

        $manager->flush();
    }

    public function loadPromo(ObjectManager $manager, $formations, $user){
        for($j = 0; $j <= 3; $j++)
        {
            $rand = rand(0,2);
            $promo = new Promo;
            $promo
                ->setName( $this->faker->word() ) 
                ->setEndyear( $this->faker->dateTime($max = 'now', $timezone = null) )
                ->setStartyear( $this->faker->dateTime($max = 'now', $timezone = null) )
                ->setFormation($formations[$rand]);
            $manager->persist($promo);
        }
        $manager->flush();
    }




    public function loadTimeslot(ObjectManager $manager, $promos){
        for( $l = 0; $l <= 14; $l++)
        {
            $rand = rand(0, 3);
            $timeslot = new Timeslot;
            $timeslot
                ->setStartdate( $this->faker->dateTime($max = 'now', $timezone = null) )
                ->setEnddate( $this->faker->dateTime($max = 'now', $timezone = null) )
                ->setCode( $this->faker->numberBetween($min = 1000, $max = 9999) )
                ->setPromo($promos[$rand]);

            $manager->persist($timeslot);
        }
        $manager->flush();
    }





    public function loadChecks(ObjectManager $manager, $users, $slot){
        foreach ($users as $user) {
            $rand = rand(0,1);
            $check = new Check;
            $check
                ->setUser($user)
                ->setTimeslot($slot)
                ->setAttendance($this->createRandomAttendance($rand));
                $manager->persist($check);
        }
    }
   
   
    public function load(ObjectManager $manager)
    {
        $this->loadFormation($manager);

            $repoFormation = $manager->getRepository(Formation::class);
            $formations = $repoFormation->findAll();

            
            $repoPromo = $manager->getRepository(Promo::class);
            $promos = $repoPromo->findAll();
            
            $this->loadTimeslot($manager, $promos);
            
            $repoTimeslot = $manager->getRepository(Timeslot::class);
            $timeslots = $repoTimeslot->findAll();
            
            $this->loadUser($manager, $promos);
            
            $repoUser = $manager->getRepository(User::class);
            $users = $repoUser->findAll();
            
            foreach ($timeslots as $slot) {
                
                $this->loadPromo($manager, $formations, $user);
                $this->loadChecks($manager, $user, $slot);
            
        }

    $manager->flush();
    }
}