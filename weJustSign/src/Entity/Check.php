<?php

namespace App\Entity;

use App\Repository\CheckRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CheckRepository::class)
 * @ORM\Table(name="`check`")
 */
class Check
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $attendance;

    /**
     * @ORM\ManyToOne(targetEntity=Timeslot::class, inversedBy="checks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $timeslot;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="checks")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAttendance(): ?string
    {
        return $this->attendance;
    }

    public function setAttendance(?string $attendance): self
    {
        $this->attendance = $attendance;

        return $this;
    }

    public function getTimeslot(): ?Timeslot
    {
        return $this->timeslot;
    }

    public function setTimeslot(?Timeslot $timeslot): self
    {
        $this->timeslot = $timeslot;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
    public function isPresent(){
        $this->setAttendance("Present");
    }
    public function isAbsent(){
        $this->setAttendance("Absent");
    }
    public function isWaiting(){
        $this->setAttendance("En attente");
    }
}
