<?php

namespace App\Entity;

use App\Repository\PromoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=PromoRepository::class)
 */
class Promo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=Timeslot::class, mappedBy="promo")
     */
    private $timeslot;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startyear;

    /**
     * @ORM\Column(type="datetime")
     */
    private $endyear;

    /**
     * @ORM\Column(type="string", length=255)

     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Formation::class, inversedBy="promo")
     * @ORM\JoinColumn(nullable=false)
     */
    private $formation;

    /**
     * @ORM\ManyToMany(targetEntity=User::class,
     * inversedBy="promos",
     * cascade={"persist"}
     * )
     */
    private $users;

    public function __construct()
    {
        $this->timeslot = new ArrayCollection();
        $this->users = new ArrayCollection();
    }
    public function __toString() {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Timeslot[]
     */
    public function getTimeslot(): Collection
    {
        return $this->timeslot;
    }

    public function addTimeslot(Timeslot $timeslot): self
    {
        if (!$this->timeslot->contains($timeslot)) {
            $this->timeslot[] = $timeslot;
            $timeslot->setPromo($this);
        }

        return $this;
    }

    public function removeTimeslot(Timeslot $timeslot): self
    {
        if ($this->timeslot->removeElement($timeslot)) {
            // set the owning side to null (unless already changed)
            if ($timeslot->getPromo() === $this) {
                $timeslot->setPromo(null);
            }
        }

        return $this;
    }

    public function getStartyear(): ?\DateTimeInterface
    {
        return $this->startyear;
    }

    public function setStartyear(\DateTimeInterface $startyear): self
    {
        $this->startyear = $startyear;

        return $this;
    }

    public function getEndyear(): ?\DateTimeInterface
    {
        return $this->endyear;
    }

    public function setEndyear(\DateTimeInterface $endyear): self
    {
        $this->endyear = $endyear;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFormation(): ?Formation
    {
        return $this->formation;
    }

    public function setFormation(?Formation $formation): self
    {
        $this->formation = $formation;

        return $this;
    }


    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addPromo($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            $user->removePromo($this);
        }

        return $this;
    }

}
